<html>
  <head>
    <style type='text/css'>
      a            { color:#0099CC }
      div.title    {
          color:#FF2244;
          font-size:1.2em;
          font-weight:bold;
          margin-top:1em;
          margin-bottom:0.5em;
          text-align:center }
      div.subtitle {
          color:#FF2244;
          font-size:0.8em;
          margin-bottom:1em;
          text-align:center }
      div.freetext {
          color:#606060;
          text-align:center }
      div.list     { color:#000000 }
    </style>
  </head>
  <body>

$ 2.0
    % Version 2.0
     _ 2014-08-01
     ! Redesigned UI!
       * Navigation Drawer for Lent Items
       * Redesigned Item Detail Screen

$ 1.88
    % Version 1.88
     _ 2014-07-28
     ! About Section!
       * Added an about section with links to social media
$ 1.87
    % Version 1.87
     _ 2014-07-17
     ! Landscape Item Details!
       * Landscape in Item Details has better Layout

$ 1.86
     % Version 1.86
     _ 2014-07-08
     ! Killing Bugs!
       * Fixed a couple bugs regarding states
       * Landscape Mode implemented
       * Should lose no data on Orientation change

$ 1.85
     % Version 1.85
     _ 2014-07-07
     ! Tablet Fixes!
       * Fixed issues with Tablets crashing
       * Made some memory optimizations

$ 1.8
     % Version 1.8
     _ 2014-07-01
     ! Price!
       * You can now add the price of an item!
       * Fixed a bug that would cause a NullPointer if you hit the Activity Switcher button

$ 1.75
      % Version 1.75
      _ 2014-06-30
      ! Bug Squashing!
        * Fixed a bug that would cause a NullPointer after taking a picture.

$ 1.7
      % Version 1.7
      _ 2014-06-27
      ! UI Enhancements!
        * List Screen now shows the a thumbnail of the item lent out.
        * Add Item screen has new Edit Text fields
        * Asynchronous loading of images on the main screen
        * Caches images loaded on the main screen.!

$ 1.6
      % Version 1.6
      _ 2014-06-20
      ! Transparency!
        * New status/action bar theme!
        * More Speed Improvements!

$ 1.5
      % Version 1.5
      _ 2014-06-20
      ! Pictures!
        * Users can now add pictures to an item
        * Pictures are automatically deleted when an item is deleted


$ 1.4
    % Version 1.4
    _ 2014-06-17
    ! Streamlined!
      * A lot of the screens have been streamlined to be more coherent
      * Speed Improvements

$ 1.3
  % Version 1.3
  _ 2014-06-12
  ! Redesign underway!
    * Lender Item Add screen has been redesigned to fit Sin-Tek Simple philosophy
    * Redesigned Change Log Colors - Utilized Holo Light
$ END_OF_CHANGE_LOG
  </body>
</html>