package com.sin.Android.widgets.licensesdialog.licenses;

import android.content.Context;
import com.sin.Android.SimpleLenderManager.R;

/**
 * Created by mav5228 on 7/28/14.
 */
public class NA extends License{


    private static final long serialVersionUID = 4854000061990891449L;

    @Override
    public String getName() {
        return "N/A";
    }

    @Override
    public String getSummaryText(final Context context) {
        return getContent(context, R.raw.na);
    }

    @Override
    public String getFullText(final Context context) {
        return getContent(context, R.raw.na);
    }

    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public String getUrl() {
        return "";
    }

}
