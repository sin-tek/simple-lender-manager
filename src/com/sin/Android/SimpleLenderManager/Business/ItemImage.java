package com.sin.Android.SimpleLenderManager.Business;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.sin.Android.SimpleLenderManager.Utility.Utility;

import java.io.File;
import java.io.IOException;

/**
 * An Item's Image to be displayed in an image view
 * <p/>
 * Created by mav5228 on 6/10/14.
 */
public class ItemImage implements Parcelable {

    private String name;

    public String getName() {
        return name;
    }

    public String getItemText() {
        return itemText;
    }

    public String getDate() {
        return date;
    }

    private String itemText;
    private String date;
    private String photoFile;
    private String filePath;

    /**
     * Sets up the items parameters of the image
     *
     * @param name     of the lendee
     * @param itemText name of the item
     * @param date     of checkout
     */
    public ItemImage(String name, String itemText, String date) {
        this.name = name;
        this.itemText = itemText;
        this.date = date;
    }

    /**
     * Sets up photo file
     *
     * @return the image file
     * @throws IOException when it can not create a file
     */
    public File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        filePath = f.getAbsolutePath();

        return f;
    }

    /**
     * Gives the file path of the image
     *
     * @return the file path
     */
    public String getPath() {
        return filePath;
    }


    public Bitmap getBitmapThumbnail(Context context, boolean addPicture) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inSampleSize = 8;
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, bmOptions);

        /*Rotate the Image */


        ExifInterface exif = null;
        try {
            exif = new ExifInterface(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;
        Matrix matrix = new Matrix();
        matrix.postRotate(rotationAngle);

        if (addPicture) {
            Utility.galleryAddPic(context, filePath);
        }

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(),
                matrix, true);

    }


    /**
     * Creates the Bitmap image from the file <br>
     * Automatically rotates an image based on its orientation as well as resizes the image<br>
     * Can also add the image to the gallery on the device
     *
     * @param context    from which is wanting the bitmap image
     * @param addPicture switch to add the picture to phone gallery
     * @return the image
     */
    public Bitmap getBitmap(Context context, boolean addPicture) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inSampleSize = 2;
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, bmOptions);


        /*Rotate the Image */


        ExifInterface exif = null;
        try {
            exif = new ExifInterface(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;
        Matrix matrix = new Matrix();
        matrix.postRotate(rotationAngle);

        if (addPicture) {
            Utility.galleryAddPic(context, filePath);
        }

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(),
                matrix, true);

    }

    /**
     * Calls getBitmap without adding the image to the gallery
     *
     * @param context from which is wanting the bitmap image
     * @return the image
     */
    public Bitmap getBitmap(Context context) {
        return getBitmap(context, false);
    }

    /**
     * Creates the file from the parameters of the item in the /SimpleLenderManager/ folder
     *
     * @return the file
     * @throws IOException if it can not create a file
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        photoFile = name + itemText + "_" + date + ".jpg";
        File albumF = getAlbumDir();
        File imageF = new File(albumF, photoFile);
        filePath = imageF.getAbsolutePath();
        return imageF;
    }

    /**
     * Gets the album directory on the device
     *
     * @return the album directory
     */
    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            storageDir = Utility.getAlbumDirectory();

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }
        } else {
        }
        return storageDir;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{
                this.filePath,
                this.itemText,
                this.date,
                this.name,
                this.photoFile
        });
    }

    public ItemImage(Parcel in){
        String[] data = new String[5];

        in.readStringArray(data);
        this.filePath = data[0];
        this.itemText = data[1];
        this.date = data[2];
        this.name = data[3];
        this.photoFile = data[4];
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ItemImage createFromParcel(Parcel in) {
            return new ItemImage(in);
        }

        public ItemImage[] newArray(int size) {
            return new ItemImage[size];
        }
    };

}

