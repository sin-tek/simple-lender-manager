package com.sin.Android.SimpleLenderManager.Business;

import android.os.Parcel;
import android.os.Parcelable;
import com.sin.Android.SimpleLenderManager.Database.LenderDataSource;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mav5228 on 12/5/13.
 */
public class Item implements Parcelable {
    private String name;
    private String lendee;
    private String dateCheckOut;
    private String dateCheckIn;
    private Long ID;
    private Float price;

    public Item(){

    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }



    public long getId() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLendee() {
        return lendee;
    }

    public void setLendee(String lendee) {
        this.lendee = lendee;
    }

    public String getDateCheckIn() {
        return dateCheckIn;
    }

    public void setDateCheckIn(String dateCheckIn) {
        this.dateCheckIn = dateCheckIn;
    }

    public String getDateCheckOut() {
        return dateCheckOut;
    }

    public String getDisplayDateCheckOut(){
        SimpleDateFormat dateParse = new SimpleDateFormat("MMddyyhhss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
        String date;

        try {
            Date dateparse = new Date();
            dateparse = dateParse.parse(dateCheckOut);
            date = dateFormat.format(dateparse);

        } catch (ParseException e) {
            date = dateCheckOut;
        }

        return date;
    }

    public void setDateCheckOut(String dateCheckOut) {
        this.dateCheckOut = dateCheckOut;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    @Override
    public String toString() {
        return "\n     Item: " + name + "\n     Lendee:  " + lendee + " \n     Date Out: " + getDisplayDateCheckOut() + "\n";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{
                this.lendee,
                this.name,
                this.dateCheckIn,
                this.dateCheckOut,
                this.ID.toString(),
                this.price.toString()
        });
    }

    public Item(Parcel in){
        String[] data = new String[6];

        in.readStringArray(data);
        this.lendee = data[0];
        this.name = data[1];
        this.dateCheckIn = data[2];
        this.dateCheckOut = data[3];
        this.ID = Long.parseLong(data[4]);
        this.price = Float.parseFloat(data[5]);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

}
