package com.sin.Android.SimpleLenderManager.Business;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.sin.Android.SimpleLenderManager.R;
import com.sin.Android.SimpleLenderManager.Screens.RoundedImageView;
import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Matthew on 6/26/2014.
 */
@LayoutId(R.layout.row_layout)
public class ListItemHolder extends ItemViewHolder<Item>{

    @ViewId(R.id.itemImage)
    RoundedImageView imageViewPerson;

    @ViewId(R.id.nameOfItem)
    TextView textViewItem;

    @ViewId(R.id.nameOfLendee)
    TextView textViewLendee;

    private Context context;
    private ImageLoader imageLoader = null;


    public ListItemHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Item item, PositionInfo positionInfo) {
        textViewItem.setText(item.getName());
        textViewLendee.setText(item.getLendee());
        setupImage(item);
        if(imageLoader == null ) {
            imageLoader = ImageLoader.getInstance();
        }
    }

    public void setContext(Context context){
        this.context = context;
    }

    private void setupImage(Item item) {

        ItemImage itemImage = new ItemImage(item.getLendee(), item.getName(),item.getDateCheckOut());
        File imageFile = null;
        try {
            imageFile = itemImage.setUpPhotoFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.icon) // resource or drawable
                .showImageForEmptyUri(R.drawable.icon) // resource or drawable
                .showImageOnFail(R.drawable.icon)
                .cacheInMemory(true)
                .considerExifParams(true)
                .cacheOnDisk(true)
                .build();
        ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
        ImageLoader imageLoader = ImageLoader.getInstance();

        if(imageFile.exists() ){


            imageLoader.displayImage("file:///" + imageFile.getAbsolutePath(), imageViewPerson, options, animateFirstListener);
            imageViewPerson.setVisibility(View.VISIBLE);
        }else{
            imageLoader.displayImage("drawable://" + R.drawable.icon, imageViewPerson, options, animateFirstListener);
            imageViewPerson.setVisibility(View.VISIBLE);
        }


    }

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
