package com.sin.Android.SimpleLenderManager.Database;

/**
 * Created by mav5228 on 12/5/13.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.sin.Android.SimpleLenderManager.Business.Item;

import java.util.ArrayList;
import java.util.List;

public class LenderDataSource {

    // Database fields
    private SQLiteDatabase database;
    private LenderDatabaseHelper dbHelper;
    private String[] allColumns = {LenderDatabaseHelper.COLUMN_ID,
            LenderDatabaseHelper.COLUMN_ITEM, LenderDatabaseHelper.COLUMN_LENDEE,
            LenderDatabaseHelper.COLUMN_DATEOUT, LenderDatabaseHelper.COLUMN_DATEIN};

    private String[] itemInfo = {LenderDatabaseHelper.COLUMN_ID,
            LenderDatabaseHelper.COLUMN_ITEM, LenderDatabaseHelper.COLUMN_LENDEE,
            LenderDatabaseHelper.COLUMN_DATEOUT, LenderDatabaseHelper.COLUMN_DATEIN, LenderDatabaseHelper.COLUMN_PRICE};

    private String[] priceColumns = {LenderDatabaseHelper.COLUMN_PRICE};

    public LenderDataSource(Context context) {
        dbHelper = new LenderDatabaseHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Item createItem(Item item) {
        /**
         * TODO: impelement other Item preferences such as Lendee and dateCheckOut
         *
         */

        ContentValues values = new ContentValues();
        values.put(LenderDatabaseHelper.COLUMN_ITEM, item.getName());
        values.put(LenderDatabaseHelper.COLUMN_LENDEE, item.getLendee());
        values.put(LenderDatabaseHelper.COLUMN_DATEOUT, item.getDateCheckOut());
        values.put(LenderDatabaseHelper.COLUMN_DATEIN, "");

        long insertId = database.insert(LenderDatabaseHelper.TABLE_LENDER, null,
                values);

        ContentValues price = new ContentValues();

        if(item.getPrice() != null){
            price.put(LenderDatabaseHelper.COLUMN_PRICE, item.getPrice());
            price.put(LenderDatabaseHelper.COLUMN_PRICE_ID, insertId);
            database.insert(LenderDatabaseHelper.TABLE_PRICE, null, price);
        }

        Cursor cursor = database.rawQuery("Select _id, item, lendee, dateOut, price " +
                " FROM lender " +
                " LEFT OUTER JOIN price_table" +
                " ON lender._id = price_table.item_id WHERE lender._id = " + insertId, null);
        cursor.moveToFirst();
        Item newItem = cursorToComment(cursor);
        cursor.close();
        return newItem;
    }

    public void deleteItem(Item item) {
        long id = item.getId();
        database.delete(LenderDatabaseHelper.TABLE_LENDER, LenderDatabaseHelper.COLUMN_ID
                + " = " + id, null);
        database.delete(LenderDatabaseHelper.TABLE_PRICE, LenderDatabaseHelper.COLUMN_PRICE_ID
                + " = " + id, null);
    }

    public List<Item> getAllItems() {
        List<Item> items = new ArrayList<Item>();


        Cursor cursor = database.rawQuery("Select _id, item, lendee, dateOut, price " +
                " FROM lender " +
                " LEFT OUTER JOIN price_table" +
                " ON lender._id = price_table.item_id", null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Item item = cursorToComment(cursor);
            items.add(item);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return items;
    }

    private Item cursorToComment(Cursor cursor) {
        Item item = new Item();
        item.setID(cursor.getLong(0));
        item.setName(cursor.getString(1));
        item.setLendee(cursor.getString(2));
        item.setDateCheckOut(cursor.getString(3));
        item.setPrice(cursor.getFloat(4));
        return item;
    }
}
