package com.sin.Android.SimpleLenderManager.Database;

/**
 * Created by mav5228 on 12/5/13.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class LenderDatabaseHelper extends SQLiteOpenHelper {

    public static final String TABLE_LENDER = "lender";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_ITEM = "item";
    public static final String COLUMN_LENDEE = "lendee";
    public static final String COLUMN_DATEOUT = "dateOut";
    public static final String COLUMN_DATEIN = "dateIn";
    public static final String TABLE_PRICE = "price_table";
    public static final String COLUMN_PRICE_ID = "item_id";
    public static final String COLUMN_PRICE = "price";

    private static final String DATABASE_NAME = "lender.db";
    private static final int DATABASE_VERSION = 2;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_LENDER + "(" + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_ITEM
            + " text not null, " + COLUMN_LENDEE + " text not null, "
            + COLUMN_DATEIN + " text, " + COLUMN_DATEOUT + " text not null);";

    private static final String PRICE_TABLE_CREATE = "create table "
            + TABLE_PRICE + "(" + COLUMN_PRICE_ID + " real, " + COLUMN_PRICE
            + " integer, " + " FOREIGN KEY (" + COLUMN_PRICE_ID + ") REFERENCES "
            + TABLE_LENDER + " (" + COLUMN_ID + "));";

    public LenderDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
        database.execSQL(PRICE_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.w(LenderDatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion);

        int upgradeTo = oldVersion + 1;
        while (upgradeTo <= newVersion)
        {
            switch (upgradeTo)
            {
                case 2:
                    db.execSQL(PRICE_TABLE_CREATE);
                    break;
            }
            upgradeTo++;
        }

    }

}