package com.sin.Android.SimpleLenderManager.Screens
        ;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.*;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.sin.Android.SimpleLenderManager.Business.*;
import com.sin.Android.SimpleLenderManager.Database.LenderDataSource;
import com.sin.Android.SimpleLenderManager.R;
import com.sin.Android.SimpleLenderManager.Utility.SystemBarTint;
import com.sin.Android.SimpleLenderManager.Utility.Utility;
import com.sin.Android.widgets.FloatingLabelView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mav5228 on 6/4/14.
 */
public class LenderAddItem extends Activity {
    private final static int TAKE_PICTURE = 1;
    private LenderDataSource datasource;
    private FloatingLabelView name, itemText, price;
    private ImageView imageView;
    private String date;
    File imageFile;
    private ItemImage itemImage = null;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setWindowAnimations(0);
        setSystemBarTint();

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyyhhmmss");
        date = dateFormat.format(new Date());
        setContentView(R.layout.add_dialog);
        datasource = new LenderDataSource(this);
        datasource.open();
        name = (FloatingLabelView) findViewById(R.id.editText);
        itemText = (FloatingLabelView) findViewById(R.id.editText2);
        imageView = (ImageView) findViewById(R.id.imageView);
        price = (FloatingLabelView) findViewById(R.id.addPrice);

    }

    @Override
    public void onBackPressed() {
        cancelDeletePicture();
        super.onBackPressed();
    }

    private void cancelDeletePicture() {
        if (imageFile != null && imageFile.exists()) {
            Utility.galleryDeletePic(this, imageFile);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.item_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_add:

                Intent itemResult = new Intent();

                Item saveItem;
                try{
                    saveItem = setupItem();
                }catch(NumberFormatException ex){
                    Toast.makeText(this,"Invalid Price", Toast.LENGTH_LONG).show();
                    return false;
                }

                itemResult.putExtra("item", datasource.createItem(saveItem));
                setResult(Activity.RESULT_OK, itemResult);

                datasource.close();
                finish();
                return true;
            case android.R.id.home:
                cancelDeletePicture();
                return super.onOptionsItemSelected(item);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Item setupItem() throws NumberFormatException {
        Item sendItem = new Item();
        sendItem.setLendee(name.getText().toString());
        sendItem.setName(itemText.getText().toString());
        sendItem.setDateCheckOut(date);

        if(price.getText().toString() == null || price.getText().isEmpty()){
            sendItem.setPrice(new Float(0.0));
        } else {
            String priceFormat = price.getText();
            sendItem.setPrice(Float.parseFloat(price.getText().toString()));
        }

        return sendItem;
    }


    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.takePicture:
                RelativeLayout takePicture = (RelativeLayout) findViewById(R.id.takePicture);
                takePicture.setBackgroundColor(getResources().getColor((R.color.onClickTakePicture)));

                if (name.getText().toString().equals("") || itemText.getText().toString().equals("")) {
                    Toast.makeText(this, "Please enter an Item name and Lendee Please", Toast.LENGTH_SHORT).show();
                } else {
                    takePicture();
                }
                takePicture.setBackgroundColor(getResources().getColor((R.color.afterClickTakePicture)));
                break;
        }

    }


    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            itemImage = new ItemImage(name.getText().toString(), itemText.getText().toString(), date);
            imageFile = itemImage.setUpPhotoFile();

            if (imageFile != null && intent.resolveActivity(getPackageManager()) != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
                startActivityForResult(intent, TAKE_PICTURE);
            }else{
                Toast.makeText(this, "No Camera on Device", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void setCameraPhoto() {

        if (itemImage.getPath() != null) {
            imageView.setImageBitmap(itemImage.getBitmap(this, true));
            imageView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TAKE_PICTURE:
                if (resultCode == RESULT_OK) {
                    setCameraPhoto();
                } else if (resultCode == RESULT_CANCELED) {

                } else {
                    Toast.makeText(this, "Error getting Image",
                            Toast.LENGTH_SHORT).show();
                }
        }

    }

    private void setSystemBarTint(){

        SystemBarTint tint = new SystemBarTint();
        tint.setSystemBarTint(this, getWindow());
    }

    /**
     * Here we store the file url as it will be null after returning from camera
     * app
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("item_image", itemImage);
        outState.putString("lendee", name.getText().toString());
        outState.putString("itemText", itemText.getText().toString());
        outState.putString("price", price.getText().toString());
        outState.putString("date", date);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        itemImage = savedInstanceState.getParcelable("item_image");
        date = savedInstanceState.getString("date");
        name.setText(savedInstanceState.getString("lendee"));
        itemText.setText(savedInstanceState.getString("itemText"));
        price.setText(savedInstanceState.getString("price"));

        if(itemImage != null){
            setCameraPhoto();
        }

    }




}
