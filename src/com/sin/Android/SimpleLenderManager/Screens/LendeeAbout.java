package com.sin.Android.SimpleLenderManager.Screens;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.sin.Android.SimpleLenderManager.Utility.SystemBarTint;
import com.sin.Android.SimpleLenderManager.R;
import com.sin.Android.widgets.htmltextview.HtmlTextView;
import com.sin.Android.widgets.licensesdialog.LicensesDialog;
import com.squareup.picasso.Picasso;

/**
 * Created by mav5228 on 7/3/14.
 */
public class LendeeAbout extends Activity {

    private TextView aboutLendee;
    private ImageView facebookImage, twitterImage, youtubeImage;

    private static String facebookURL = "https://badge.facebook.com/badge/667253406673042.2790.1241876288.png";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lendee_about);

        aboutLendee = (TextView) findViewById(R.id.about);
        try {
            aboutLendee.setText(getResources().getString(R.string.app_name) + " v. " + this.getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("About", "could not get version name from manifest!");
            e.printStackTrace();
        }

        facebookImage = (ImageView) findViewById(R.id.facebookBadge);
        twitterImage = (ImageView) findViewById(R.id.twitter);
        youtubeImage = (ImageView) findViewById(R.id.youtube);
        setSystemBarTint();
        setSocialImages();


    }

    private void setSocialImages() {
        Picasso.with(this).load(R.drawable.facebook).resize(200, 200).centerInside().into(facebookImage);
        Picasso.with(this).load(R.drawable.twitter).resize(200, 200).centerInside().into(twitterImage);
        Picasso.with(this).load(R.drawable.youtube).resize(200, 200).centerInside().into(youtubeImage);

        HtmlTextView text = (HtmlTextView) findViewById(R.id.developerText);
        text.setHtmlFromRawResource(this, R.raw.about_text, true);
    }


    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.license:
                new LicensesDialog(this, R.raw.notices, false, false).show();
                break;
            case R.id.facebookBadge:
                openWebURL("http://www.facebook.com/xsinisterdragonx");
                break;
            case R.id.twitter:
                openWebURL("http://www.twitter.com/sinisterdragon");
                break;
            case R.id.youtube:
                openWebURL("http://www.youtube.com/xsinisterdragonx");
                break;
        }

    }

    public void openWebURL(String inURL) {
        Intent browse = new Intent(Intent.ACTION_VIEW, Uri.parse(inURL));
        startActivity(browse);
    }

    private void setSystemBarTint() {

        SystemBarTint tint = new SystemBarTint();
        tint.setSystemBarTint(this, getWindow());
    }


}
