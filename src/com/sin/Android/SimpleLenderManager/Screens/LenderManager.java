package com.sin.Android.SimpleLenderManager.Screens;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.sin.Android.SimpleLenderManager.Business.ChangeLog;
import com.sin.Android.SimpleLenderManager.Business.Item;
import com.sin.Android.SimpleLenderManager.Business.ItemImage;
import com.sin.Android.SimpleLenderManager.Business.ListItemHolder;
import com.sin.Android.SimpleLenderManager.Database.LenderDataSource;
import com.sin.Android.SimpleLenderManager.R;
import com.sin.Android.SimpleLenderManager.Utility.SystemBarTint;
import com.sin.Android.SimpleLenderManager.Utility.Utility;
import com.sin.Android.SimpleLenderManager.Utility.Vibrate;
import com.sin.Android.widgets.FloatingActionButton;
import uk.co.ribot.easyadapter.EasyAdapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LenderManager extends Activity {
    final Context context = this;
    private LenderDataSource dataSource;
    private ChangeLog cl = null;
    private Item lastClicked;
    private int totalCountNum;
    private TextView totalCount;
    private FloatingActionButton mFab;
    private List<Item> values;
    private ListView itemList;
    private CharSequence drawerTitle = "Item List";
    private CharSequence detailTitle = "Item Detail";
    private CharSequence appTitle = "Lendee";
    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private LenderItemFragment fragment;
    private boolean deletedFromFragment = false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


        getWindow().setWindowAnimations(0);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .threadPoolSize(3)
                .memoryCacheExtraOptions(300, 300) // default = device screen dimensions
                .diskCacheExtraOptions(300, 300, null)
                .diskCacheFileCount(100)
                .memoryCacheSize(2 * 1024 * 1024)
                .build();
        ImageLoader.getInstance().init(config);


        totalCount = (TextView) findViewById(R.id.totalNum);
        mFab = (FloatingActionButton) findViewById(R.id.fabbutton);

        cl = new ChangeLog(this);
        if (cl.firstRunEver() || cl.firstRun()) {
            cl.getFullLogDialog().show();
        }

        dataSource = new LenderDataSource(this);
        dataSource.open();
        values = dataSource.getAllItems();
        totalCountNum = values.size();
        setSystemBarTint();
        updateTotalCount();

        setupList();

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent k = new Intent(context, LenderAddItem.class);
                startActivityForResult(k, 1);
            }
        });
        mFab.listenTo(itemList);
        mFab.hide(false);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.drawable.ic_drawer, R.string.app_name, R.string.app_name){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                mFab.hide(true);
                setActionBarOpen();

                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                mFab.hide(false);
                getActionBar().setTitle(drawerTitle);
                invalidateOptionsMenu();
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerLayout.openDrawer(Gravity.START);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void setActionBarOpen(){
        if(fragment == null){
            getActionBar().setTitle(appTitle);
        }else{
            getActionBar().setTitle(detailTitle);
        }
    }



    private void setupList() {
        EasyAdapter<Item> adapter = new EasyAdapter<Item>(this, ListItemHolder.class, values);

        itemList = (ListView) findViewById(R.id.item_list);
        itemList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> av, View v, int pos, long id) {
                return onLongListItemClick(v, pos, id);
            }

        });

        final Context context1 = this;
        itemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Vibrate.vibrate(context1, Vibrate.TWENTY);
                lastClicked = (Item) parent.getAdapter().getItem(position);
                fragment = LenderItemFragment.newInstance(lastClicked);
                FragmentManager fragmentManger = getFragmentManager();
                fragmentManger.beginTransaction().replace(R.id.drawer_content, fragment).commit();
                drawerLayout.closeDrawer(Gravity.START);
            }
        });
        itemList.setAdapter(adapter);


    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void setSystemBarTint() {

        SystemBarTint tint = new SystemBarTint();
        tint.setSystemBarTint(this, getWindow());
    }

    private void updateTotalCount() {
        totalCount.setText(Integer.toString(totalCountNum));

    }


    public boolean onLongListItemClick(View v, int position, long id) {
        final Context context = this;
        final Item item = (Item) itemList.getAdapter().getItem(position);
        deleteItem(item);
        return true;
    }

    private boolean deleteItem(final Item item){

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Delete?");
        alert.setMessage("Are you sure you want to delete?");

        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EasyAdapter<Item> adapter = (EasyAdapter<Item>) itemList.getAdapter();

                ItemImage itemImage = new ItemImage(item.getLendee(), item.getName(), item.getDateCheckOut());
                File deletePicture = null;
                try {
                    deletePicture = itemImage.setUpPhotoFile();
                    Utility.galleryDeletePic(context, deletePicture);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                dataSource.deleteItem(item);
                values.remove(item);
                adapter.notifyDataSetChanged();
                deletePicture(item);
                totalCountNum--;
                updateTotalCount();
                deletedFromFragment = true;
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                deletedFromFragment = false;
            }
        });


        alert.show();
       return deletedFromFragment;
    }

    private void deletePicture(Item item) {
        File file = new File(Utility.getAlbumDirectory(), Utility.buildFileNameFromItem(item));
        if (file.exists()) {
            Utility.galleryDeletePic(this, file);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_about:
                Intent about = new Intent(context, LendeeAbout.class);
                startActivity(about);
                return true;
            case R.id.action_delete:
                deleteItemFromFragment(lastClicked);



            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void deleteItemFromFragment(final Item item){

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Delete?");
        alert.setMessage("Are you sure you want to delete?");

        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EasyAdapter<Item> adapter = (EasyAdapter<Item>) itemList.getAdapter();

                ItemImage itemImage = new ItemImage(item.getLendee(), item.getName(), item.getDateCheckOut());
                File deletePicture = null;
                try {
                    deletePicture = itemImage.setUpPhotoFile();
                    Utility.galleryDeletePic(context, deletePicture);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                dataSource.deleteItem(item);
                values.remove(item);
                adapter.notifyDataSetChanged();
                deletePicture(item);
                totalCountNum--;
                updateTotalCount();

                FragmentManager fragmentManger = getFragmentManager();
                fragmentManger.beginTransaction().remove(fragment).commit();
                fragment = null;
                setActionBarOpen();
                invalidateOptionsMenu();
                drawerLayout.openDrawer(Gravity.START);

            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });


        alert.show();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu){
        boolean drawerOpen = drawerLayout.isDrawerOpen(R.id.lef_drawer);
        if(!drawerOpen && fragment != null){
            menu.clear();
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.item_details_menu, menu);
        }else{
            menu.clear();
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.main_activity_actions, menu);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("totalCount", totalCountNum);


        if(fragment != null){
            outState.putBoolean("isFragment", true);
            FragmentManager test = getFragmentManager();
            test.putFragment(outState, "fragment", fragment);
        }else{
            outState.putBoolean("isFragment", false);
        }

        outState.putParcelableArrayList("values", (ArrayList<Item>) values);

    }

    /*
     * Here we restore the fileUri again
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        values = savedInstanceState.getParcelableArrayList("values");
        totalCountNum = savedInstanceState.getInt("totalCount");

        Boolean isFragment = savedInstanceState.getBoolean("isFragment");
        if(isFragment){
            FragmentManager test = getFragmentManager();
            fragment = (LenderItemFragment) test.getFragment(savedInstanceState, "fragment");
            lastClicked = fragment.getItemFromFragment();
        }

        totalCount.setText(Integer.toString(totalCountNum));
        setupList();
        setActionBarOpen();
        invalidateOptionsMenu();

    }


    @Override
    protected void onResume() {
        dataSource.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        dataSource.close();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                EasyAdapter<Item> adapter = (EasyAdapter<Item>) itemList.getAdapter();
                Item result = (Item) data.getExtras().get("item");
                values.add(result);
                adapter.notifyDataSetChanged();
                totalCountNum++;
                updateTotalCount();
            }
        }
    }

}


