package com.sin.Android.SimpleLenderManager.Screens;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.sin.Android.SimpleLenderManager.Business.Item;
import com.sin.Android.SimpleLenderManager.Business.ItemImage;
import com.sin.Android.SimpleLenderManager.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

/**
 * Created by mav5228 on 6/5/14.
 */
public class LenderItemFragment extends Fragment {
    private Item item;
    private TextView nameOfItem, lendee, dateOut, price, priceLabel;
    private ProgressBar spinner;
    private ImageView image;
    private ItemImage itemImage;
    Context context;
    public static int RESULT_DELETE = 99;

    public static LenderItemFragment newInstance(Item item) {
        LenderItemFragment f = new LenderItemFragment();

        //Put Item into Fragment
        Bundle args = new Bundle();
        args.putParcelable("item", item);
        f.setArguments(args);
        return f;
    }

    private Item getItem() {
        return getArguments().getParcelable("item");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.item_details, container, false);
        context = container.getContext();
        item = getItem();
        onAfterCreate(rootView);
        return rootView;

    }

    public void onAfterCreate(View root) {
        nameOfItem = (TextView) root.findViewById(R.id.nameofItem);
        lendee = (TextView) root.findViewById(R.id.lendee);
        price = (TextView) root.findViewById(R.id.detailPrice);
        dateOut = (TextView) root.findViewById(R.id.dateOut);
        image = (ImageView) root.findViewById(R.id.imageView);
        priceLabel = (TextView) root.findViewById(R.id.priceLabel);

        spinner = (ProgressBar) root.findViewById(R.id.progressBar);

        setUpLabels();
        setupImage();

    }

    private void setUpLabels() {
        nameOfItem.setText(item.getName());
        lendee.setText(item.getLendee());
        dateOut.setText(item.getDisplayDateCheckOut());

        Float priceOfItem = item.getPrice();
        if (priceOfItem == 0.0) {

            priceLabel.setVisibility(View.INVISIBLE);
            price.setVisibility(View.INVISIBLE);
        } else {
            price.setText("$" + Float.toString(priceOfItem));
        }

    }


    private void setupImage() {

        image.setVisibility(View.VISIBLE);

        itemImage = new ItemImage(item.getLendee(), item.getName(), item.getDateCheckOut());
        File imageFile = null;

        try {
            imageFile = itemImage.setUpPhotoFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        spinner.setVisibility(View.GONE);

        if (imageFile.exists()) {


            Picasso.with(context).load("file://" + itemImage.getPath()).resize(1000, 1000).centerInside().into(image, new Callback() {
                @Override
                public void onSuccess() {
                    image.setVisibility(View.VISIBLE);

                }

                @Override
                public void onError() {
                    Toast.makeText(context, "Error Loading Image", Toast.LENGTH_SHORT).show();
                    spinner.setVisibility(View.GONE);
                }
            });
        }


        image.setVisibility(View.VISIBLE);
    }

    public Item getItemFromFragment() {
        return item;

    }

}
