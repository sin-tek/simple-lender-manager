package com.sin.Android.SimpleLenderManager.Utility;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;
import com.sin.Android.SimpleLenderManager.R;

/**
 * Created by mav5228 on 7/3/14.
 */
public class SystemBarTint {


    public void setSystemBarTint(Activity activity, Window window){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            setTranslucentStatus(true, window);
            SystemBarTintManager tintManager = new SystemBarTintManager(activity);
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setStatusBarTintResource(R.color.actionBarBackground);
        }else{
            return;
        }


    }

    public void setSystemBarTint(Activity activity, Window window, int color){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            setTranslucentStatus(true, window);
            SystemBarTintManager tintManager = new SystemBarTintManager(activity);
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setStatusBarTintColor(color);
        }else{
            return;
        }


    }

    @TargetApi(19)
    private void setTranslucentStatus(boolean on, Window window) {
        Window win = window;
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);

    }
}
