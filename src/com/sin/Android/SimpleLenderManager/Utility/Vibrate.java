package com.sin.Android.SimpleLenderManager.Utility;

import android.content.Context;
import android.os.Vibrator;

/**
 * Just a simple wrapper class to cause Vibrations
 * Created by mav5228 on 7/29/14.
 */
public class Vibrate {

    public static final Integer TWENTY = 20;

    public static void vibrate(Context context, int timeInMs){
        Vibrator vibe = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibe.vibrate(timeInMs);
    }
}
